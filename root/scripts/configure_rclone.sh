rclone version

echo "[$(date '+%Y-%m-%d|%H:%M:%S')] Setup rclone.conf..."

test -e ${RCLONE_CONFIG}

if [ $? -eq 1 ]
then 
    echo "[S3]" >> ${RCLONE_CONFIG}
    echo "type = s3" >> ${RCLONE_CONFIG}
    echo "provider = ${PROVIDER}" >> ${RCLONE_CONFIG}
    echo "env_auth = false" >> ${RCLONE_CONFIG}
    echo "region = ${REGION}" >> ${RCLONE_CONFIG}
    echo "endpoint = ${ENDPOINT}" >> ${RCLONE_CONFIG}
    echo "acl = private" >> ${RCLONE_CONFIG}
    echo "access_key_id = ${ACCESS_KEY_ID}" >> ${RCLONE_CONFIG}
    echo "secret_access_key = ${SECRET_ACCESS_KEY}" >> ${RCLONE_CONFIG}
    echo "force_path_style = ${FORCE_PATH_STYLE}" >> ${RCLONE_CONFIG}
else
    echo "[$(date '+%Y-%m-%d|%H:%M:%S')] Found custom rclone.conf!"
fi

echo "[$(date '+%Y-%m-%d|%H:%M:%S')] Setup complete!"

echo "[$(date '+%Y-%m-%d|%H:%M:%S')] ${RCLONE_CONFIG}:"
cat ${RCLONE_CONFIG} | sed -r "s~secret_access_key = .+$~secret_access_key = ########~"
echo "\n"
