# Docker Volume Backup
GitLab: [https://gitlab.com/docker_repos/docker-volume-backup](https://gitlab.com/docker_repos/docker-volume-backup)  
DockerHub: [https://hub.docker.com/r/mega349/docker-volume-backup](https://hub.docker.com/r/mega349/docker-volume-backup)
## Private Backup tool, use at your own risk!
### Main components:
- [Tar](https://www.gnu.org/software/tar/)
- [Rclone](https://rclone.org/)
- cron

### Primary made for Swarm mode

(just edit the compose file if you want standalone)  
The "AWS Settings" environment variables are just made for S3 Storage ([Wasabi](https://wasabi.com/) S3 endpoint in my case), if you wana use another storage endpoint ignore them and override `/data/rclone.conf` with an valid [Rclone](https://rclone.org/) Configuration file.

### compose.yml
```yaml
version: '3.8'

services:
  app:
    image: mega349/docker-volume-backup:latest
    deploy:
      mode: global
      restart_policy:
        condition: on-failure
    volumes:
      - /var/lib/docker/volumes:/volumes:ro                 # Mount Docker volumes
      - /etc/hostname:/data/hostname:ro                     # Mount (bind) hostname (if not set, default is "docker-volume-backup")
      # - /your/path/rclone.conf:/data/rclone.conf:ro       # mount (bind) custom rclone.conf (override default / not needed by default)
    environment:
                 # GENERAL Settings 
      - BACKUPTIME_CRON="0 5 * * *"                         # Backup time in cron syntax
      - TZ=Europe/Amsterdam                                 # Set timezone
      - DELETEAFTERDAYS=30                                  # Delete after x days (0 is disabled)
      # - RCLONE_CONFIG=/data/rclone.conf                   # Custom rclone.conf path in docker (not needed by default)

                # AWS Settings (Ignore if rclone.conf is mounted)
      - ACCESS_KEY_ID=*******                               # (Your AWS_ACCESS_KEY_ID)
      - SECRET_ACCESS_KEY=*******                           # (Your AWS_SECRET_ACCESS_KEY)
      - BUCKET=docker-volume-backup                         # (Your Bucket Name)
      - ENDPOINT=s3.eu-central-1.wasabisys.com              # (your Bucket Endpoint)
      - REGION=eu-central-1                                 # (Your Bucket Region)
      - PROVIDER=Wasabi                                     # (Your Provider)
      - FORCE_PATH_STYLE=true                               # (Use Path Style? true/false)
```

### Planed:
- [ ] Ignore List for Volumes (Regex?)
- [ ] Changeable endpoint Name (Currently [S3] even its not an S3-endpoint) **`ATTENTION! - If you replace the rclone.conf`**
