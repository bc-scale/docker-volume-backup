sh /scripts/configure_rclone.sh

env > /etc/environment
echo "${BACKUPTIME_CRON} root sh /scripts/main.sh>/proc/1/fd/1 2>/proc/1/fd/1" | sed -r "s~\"~~" | sed -r "s~\"~~" >> /etc/crontab

echo "[$(date '+%Y-%m-%d|%H:%M:%S')] Waiting for cron [${BACKUPTIME_CRON}]"
cron -f
