echo "[$(date '+%Y-%m-%d|%H:%M:%S')] Backup started..."

export VOLUMESDIR="/volumes"
export HOSTNAME="$(cat /data/hostname)"

mkdir -p -v /temp/${HOSTNAME}

echo "[$(date '+%Y-%m-%d|%H:%M:%S')] Volumes:"
ls -lah /volumes/
echo "\n"

for f in ${VOLUMESDIR}/*; do
    if [ -d "$f" ]; then
        echo "[$(date '+%Y-%m-%d|%H:%M:%S')] processing ${f}..."
        export DIRNAME=$(basename ${f})
        if [ -z $(echo "${DIRNAME}" | sed -r "s~.{64,}~~") ]
        then 
            echo "[$(date '+%Y-%m-%d|%H:%M:%S')] The dir name length is too long (>= 64), this is probably not a persistent named volume."
        else 
            export FILENAME="${DIRNAME}_$(date '+%Y%m%d%H%M%S').tar.gz"
            tar cpfz "/temp/${HOSTNAME}/${FILENAME}" -C "${f}" .  
            rclone copy -P "/temp/${HOSTNAME}/${FILENAME}" "S3:/${BUCKET}/${HOSTNAME}/${DIRNAME}/"
            if [ ${DELETEAFTERDAYS} -ne 0 ]
            then
                rclone delete -P -v --min-age ${DELETEAFTERDAYS}d "S3:/${BUCKET}/${HOSTNAME}/${DIRNAME}/"
            fi
        fi
    fi
done
rm -v -r /temp/*
echo "[$(date '+%Y-%m-%d|%H:%M:%S')] Backup complete!"
